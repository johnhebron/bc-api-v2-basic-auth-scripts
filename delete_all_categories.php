<?php
/*
    This script is flawed in that it grabs a list of the categories
    sorted by id and then tries to delete them one by one.

    The problem is that it doesn't take into account whether the 
    category has sub categories or not. So, by deleting one 
    category, if it has sub categories then you are deleting
    them as well, thus messing up the category count.

    Could be fixed by sorting the categories based on their structure
    so that only bottom level categories are listed first, then 
    second level, so on and so forth.

    This would also speed up the script since deleting a top level
    category ends up taking longer since it has to delete its 
    sub level categories as well.

    Oh, and you can't delete a category if it contains a product that
    doesn't exist in another category.
*/


    // Turn on Error Reporting in PHP //
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    // Require the composer autoloader 
    // and the Environment variables
    require __DIR__ . '/vendor/autoload.php';
    require __DIR__ . '/.env';
    
    // Set namespace
    use Bigcommerce\Api\Client as Bigcommerce;

    // Configure connection to BigCommerce store
    // using Basic Auth and API v2
    Bigcommerce::configure(array(
        'store_url' => getenv('STORE_URL'),
        'username' => getenv('USERNAME'),
        'api_key' => getenv('API_KEY')
    ));

    // Open a Try/Catch block 
    // to get the count of categories
    try {
        $count = Bigcommerce::getCategoriesCount();
    } catch(Bigcommerce\Api\Error $error) {
        echo $error->getCode();
        echo $error->getMessage();
    }

    // Set the number of pages based on
    // a default of 50 categories per page
    $pages = $count / 50;
?>

<html>
<head>
</head>
<body>
  <h1>Deleting BC Categories via API V2</h1>

        <table>
            <tr>
                <th>Category ID</th>
                <th>Category Name</th>
                <th>Deleted?</th>
            </tr>
        
        <?
        // Delete all of the categories
        for ($i=0; $i < $pages ; $i++) { 
            $filter = array("page" => $i + 1);
            $categories = Bigcommerce::getCategories($filter);

            foreach ($categories as $category) {
                // Set a Try/Catch block to delete each category
                try{
                    $delete = Bigcommerce::deleteCategory($category->id);
                    var_dump($delete);
                    ?>
                    <tr>
                        <td><?= $category->id ?></td>
                        <td><?= $category->name ?></td>
                        <td>Yes</td>
                    </tr>
                    <?php
                }catch(Exception $e){
                    ?>
                    <tr>
                        <td><?= $category->id ?></td>
                        <td><?= $category->name ?></td>
                        <td><?= $e ?></td>
                    </tr>
                    <?php
                }
            } 
        }
        ?>
        </table>
    </body>
</html>