# bc-v2-basic_auth-scripts
A collection of BigCommerce v2 API scripts using basic authentication to play with

# Set Up
- Clone this Repo
- Run composer install
- Rename .env.example to remove the example
- Edit .env and add in your basic auth credentials
- Make sure the store url does NOT include the /api/v2