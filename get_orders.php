<?php
    // Turn on Error Reporting in PHP //
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    // Require the composer autoloader 
    // and the Environment variables
    require __DIR__ . '/vendor/autoload.php';
    require __DIR__ . '/.env';
    
    // Set namespace
    use Bigcommerce\Api\Client as Bigcommerce;

    // Configure connection to BigCommerce store
    // using Basic Auth and API v2
    Bigcommerce::configure(array(
        'store_url' => getenv('STORE_URL'),
        'username' => getenv('USERNAME'),
        'api_key' => getenv('API_KEY')
    ));

    // Open a Try/Catch block 
    // to get 50 most recent orders from Store
    try {
        $orders = Bigcommerce::getOrders();
    } catch(Bigcommerce\Api\Error $error) {
        echo $error->getCode();
        echo $error->getMessage();
    }

?>

<html>
<head>
</head>
<body>
	<h1>Testing BC Orders via API V2</h1>

        <table>
            <tr>
                <th>Order ID</th>
                <th>Date Created</th>
                <th>Order Status</th>
            </tr>
        
        <?
        foreach($orders as $order) { ?>
            <tr>
                <td><?= $order->id ?></td>
                <td><?= $order->date_created ?></td>
                <td><?= $order->status ?></td>
            </tr>
        <?php } ?>
        </table>
    </body>
</html>